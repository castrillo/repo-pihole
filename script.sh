#!/bin/bash
echo "..starting testing"
filename='manual-blocklist.txt'
c=0

# Color variables
red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
blue='\033[0;34m'
magenta='\033[0;35m'
cyan='\033[0;36m'
# Clear the color after that
clear='\033[0m'

while read line; do
    testing=$(whois $line | grep -i "Domain not found")
    if [ $? == 0 ]; then
    c=$((c+1))
    echo $line >> output.txt
    fi
done < $filename

count=$(cat $filename | wc -l)
echo "number of domain into the list:" $count

if [ $c -ge 1 ]; then
echo -e "${red}found $c domains with no response!${clear}!"
echo -e "${yellow}please remove the following domains${clear}" 
cat -n output.txt
exit 1
else  
echo -e "${green}Test passed${clear}"
exit 0
fi